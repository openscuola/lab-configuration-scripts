#!/bin/bash
# vim: tabstop=2 shiftwidth=2

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if [ $(whoami) != "root" ]; then
	echo Sono necessari i permessi di root
	exit 1
fi

################################################################################
############################ Variabili dello script ############################
################################################################################

# Cartella generale openscuola, con sottocartelle script e modelli
openscuola_main_dir="/opt/openscuola"
# Cartella con i soli script OpenScuola
script_dir="${openscuola_main_dir}/lab-configuration-scripts"
# Cartella modelli Libreoffice
libreoffice_dir="${openscuola_main_dir}/modelli-libreoffice"
# Cartella configurazione. Qui verranno salvati i file di configurazione laboratorio
config_dir="${openscuola_main_dir}/config"

# File da importare contenente le variabili di configurazione del laboratorio
config_file="${config_dir}/labconfig.sh"
# File immagine di sfondo
background_img="/usr/share/backgrounds/liberoMareReversedOpenScuola.jpg"

################################################################################
################# Variabili di configurazione del laboratorio ##################
################################################################################

# Controllo se il file delle variabili di configurazione esiste
if ! [ -f "${config_file}" ]; then
  echo "Il file ${config_file} non esiste!"
  exit 1
fi

# Controllo se il file delle variabili di configurazione del laboratorio contiene solo commenti o righe del tipo Variabile=Valore
if grep -Eqv "^\s*$|^\s*#|^\s*[^ ]*=\"[^;]*\"$" "${config_file}"; then
  echo "Il file ${config_file} non e' corretto!"
  exit 1
fi

# Importa le variabili di configurazione del laboratorio
source "${config_file}"

################################################################################
####################### Aggiornamento script OpenScuola ########################
################################################################################

# Potrebbe andare in conflitto se il file 30update.sh viene aggiornato _mentre_ viene eseguito?
# Per ora, aggiornare questo repo manualmente
# echo
# echo Aggiornamento script openscuola
# echo
# cd "${script_dir}"
# git restore .
# git pull

echo
echo Aggiornamento modelli Libreoffice
echo
cd "${libreoffice_dir}"
git checkout .
# git restore . # comando restore presente solo nelle nuove versioni di git
git pull

# Copia i modelli aggiornati nelle home di tutti gli utenti e del docente
cp -R "${libreoffice_dir}/." "/home/${username_docente}/Modelli" # Aggiunta dei modelli
for i in $(seq ${n_utenti}); do
  username_client="${username_client_base}$(printf "%02d" ${i})"
  cp -R "${libreoffice_dir}/." "/home/${username_client}/Modelli" # Aggiunta dei modelli
done


################################################################################
############################## Aggiornamento sfondo  ###########################
################################################################################

echo
echo Scaricamento sfondo aggiornato
echo
cd /tmp
wget -t 1 -T 20 -nv -O .back_tmp_$$.jpg "http://linux.studenti.polito.it/wp/wp-content/uploads/liberoMareReversedOpenScuola.jpg"
if [ $? -eq 0 ]; then
	mv .back_tmp_$$.jpg "$background_img"
else
	rm -f .back_tmp_$$.jpg
fi

################################################################################
############################ Esecuzione 10software.sh  #########################
################################################################################

cd "${script_dir}"
echo
echo Esecuzione 10software.sh
echo
./10software.sh
