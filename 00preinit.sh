#!/bin/bash

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>


################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if ! [ $(id -u) = 0 ]; then
  echo "Sono necessari i permessi di root!"
  exit 1
fi

################################################################################
############################ Variabili dello script ############################
################################################################################

# File di configurazione delle sources di apt
apt_sources_conf="/etc/apt/sources.list"
# File di configurazione fstab
fstab_conf="/etc/fstab"
# File di configurazione aggiornamenti automatici
auto_upgrades_conf="/etc/apt/apt.conf.d/20auto-upgrades"
# File di configurazione di nuove versioni di Ubuntu
release_upgrades_conf="/etc/update-manager/release-upgrades"
# File di configurazione apport
apport_conf="/etc/default/apport"
# Cartella generale openscuola, con sottocartelle script e modelli
openscuola_main_dir="/opt/openscuola"
# Cartella con i soli script OpenScuola
script_dir="${openscuola_main_dir}/lab-configuration-scripts"
# Cartella modelli Libreoffice
libreoffice_dir="${openscuola_main_dir}/modelli-libreoffice"
# Cartella configurazione. Qui verranno salvati i file di configurazione laboratorio
config_dir="${openscuola_main_dir}/config"

# Etichette delle partizioni
root_lbl="OSC2004_ROOT"
home_lbl="OSC2004_HOME"
swap_lbl="LINUX_SWAP"

################################################################################
######################## Gestione parametri dello script #######################
################################################################################

while getopts "rui" parametro
do
  case ${parametro} in
    r)
      echo "Ripristino..."
      mv "${apt_sources_conf}.bak" "${apt_sources_conf}"
      mv "${fstab_conf}.bak" "${fstab_conf}"
      mv "${auto_upgrades_conf}.bak" "${auto_upgrades_conf}"
      mv "${release_upgrades_conf}.bak" "${release_upgrades_conf}"
      mv "${apport_conf}.bak" "${apport_conf}"
      exit 0
      ;;
    u)
      echo "Updating libreoffice template repository"
      cd "${libreoffice_dir}"
      git checkout .
      # git restore . comando restore presente solo nelle nuove versioni di git
      git pull
      exit 0
      ;;
    i)
      ################################################################################
      ############################ Configurazioni iniziali ###########################
      ################################################################################

      # Abilitazione del repository partner
      cp "${apt_sources_conf}" "${apt_sources_conf}.bak"
      sed -i '/^# deb .*partner$/s/^# //' "/etc/apt/sources.list"

      # Configurazione delle ACL
      cp "${fstab_conf}" "${fstab_conf}.bak"
      sed -Ei '/,acl/! {s:(/home[^d]*defaults):\1,acl:}' "${fstab_conf}"
      mount -o remount /home

      # Impostazione label su disco
      e2label $(df / | tail -n 1 | cut -d ' ' -f 1) ${root_lbl}
      e2label $(df /home | tail -n 1 | cut -d ' ' -f 1) ${home_lbl}
      device_swap=$(tail -n 1 /proc/swaps | cut -d ' ' -f 1)
      swapoff -a
      swaplabel -L ${swap_lbl} "${device_swap}"
      swapon -L ${swap_lbl}

      # Impostazione label su /etc/fstab
      sed -Ei "s/^UUID=[^ \t]+([ \t]+\/[ \t]+)/LABEL=${root_lbl}\1/" "${fstab_conf}"
      sed -Ei "s/^UUID=[^ \t]+([ \t]+\/home[ \t]+)/LABEL=${home_lbl}\1/" "${fstab_conf}"
      sed -Ei "s/^UUID=[^ \t]+([ \t]+none[ \t]+swap)/LABEL=${swap_lbl}\1/" "${fstab_conf}"

      # Disabilitazione della notifica aggiornamenti
      cp "${auto_upgrades_conf}" "${auto_upgrades_conf}.bak"
      sed -i 's/Update-Package-Lists\ "1"/Update-Package-Lists\ "0"/' "${auto_upgrades_conf}"
      sed -i 's/Unattended-Upgrade\ "1"/Unattended-Upgrade\ "0"/' "${auto_upgrades_conf}"

      # Disabilitazione della notifica di nuove versioni di Ubuntu
      cp "${release_upgrades_conf}" "${release_upgrades_conf}.bak"
      sed -i 's/[pP]rompt=.*$/prompt=never/' "${release_upgrades_conf}"

      # Disabilitazione delle notifiche di segnalazione degli errori
      cp "${apport_conf}" "${apport_conf}.bak"
      sed -i 's/enabled=1/enabled=0/' "${apport_conf}"

      # Scaricamento degli script e del file delle variabili di configurazioni del laboratorio
      cd "${script_dir}"
      chmod 744 ./*.sh

      mkdir -p "${config_dir}"
      cp --no-clobber labconfig_TEMPLATE.sh "${config_dir}/labconfig.sh"


      # Scaricamento dei modelli
      cd "${openscuola_main_dir}"
      git clone https://gitlab.com/openscuola/modelli-libreoffice.git

      # Cambio timeout del servizio che attende che la rete sia online
      mkdir -p /etc/systemd/system/systemd-networkd-wait-online.service.d/
      cat > /etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf << EOF
[Service]
# modifica del timeout (da 1min 30s)
# ExecStart deve essere svuotato prima di ridefinirlo (i servizi oneshot possono
# avere piu' ExecStart)
ExecStart=
ExecStart=/lib/systemd/systemd-networkd-wait-online --timeout=1
EOF

      # cambio timeout di grub
      sed -Ei 's/^GRUB_TIMEOUT=10$/GRUB_TIMEOUT=5/' /etc/default/grub
      # Rimozione memtest dal menu di GRUB
      chmod -x /etc/grub.d/20_memtest86+
      # rigenerazione file di configurazione di GRUB per applicare le modifiche
      update-grub
      exit 0
      ;;
    [?])
      echo "Parametro non riconosciuto!"
      exit 1
      ;;
  esac
done

if [ $# -eq 0 ]; then
  echo "$0 -i: preinit del sistema openscuola"
  echo "$0 -r: ripristina da un precedente -i"
  echo "$0 -u: scarica l'ultima versione dello script di update"
  exit 1 
fi
