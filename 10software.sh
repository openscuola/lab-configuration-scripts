#!/bin/bash
# vim: tabstop=2 shiftwidth=2 expandtab

# Copyright © 2014,2015,2016,2017,2018,2019,2020,2021 Alessandro Ugo, Marco Sciuto, Filippo Bonazzi, Marco De Benedictis, Carmelo Riolo, Giuseppe D'Andrea, Massimo Gismondi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>

################################################################################
######################## Controllo dei permessi di root ########################
################################################################################

if ! [ $(id -u) = 0 ]; then
  echo "Sono necessari i permessi di root!"
  exit 1
fi

################################################################################
############################# Variabili dello script ###########################
################################################################################
scratch3_deb_url='https://linux.studenti.polito.it/static_resources/openscuola/deb_programmi/scratch-desktop_3.4.0_amd64.deb'
freemind_installer_url='https://linux.studenti.polito.it/static_resources/openscuola/deb_programmi/freemind_installer.sh'
cmaptools_installer_url='https://linux.studenti.polito.it/static_resources/openscuola/deb_programmi/cmaptools_installer.tgz'

################################################################################
############################# Repository aggiuntivi ############################
################################################################################

# Repository GeoGebra
echo "deb http://www.geogebra.net/linux/ stable main" > "/etc/apt/sources.list.d/geogebra.list"
apt-key adv --fetch-keys "https://static.geogebra.org/linux/office@geogebra.org.gpg.key"
# Repository Google Chrome
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > "/etc/apt/sources.list.d/google-chrome.list"
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
# Repository avidemux
add-apt-repository -y ppa:ubuntuhandbook1/avidemux
# Repository OpenShot
add-apt-repository -y ppa:openshot.developers/ppa

################################################################################
####################### Upgrade dei pacchetti installati #######################
################################################################################

# Aggiornamento dell'indice dei pacchetti
apt update
# Aggiornamento dei pacchetti
apt upgrade

################################################################################
###################### Installazione pacchetti aggiuntivi ######################
################################################################################

# Installazione di:
#  (riga 1) metapacchetti edu nuovi
#  (riga 2) pacchetti precedentemente in ubuntu-edu-*, ora da aggiungere a parte
#  (riga 3) applicativi accessori; applicativi richiesti dai docenti
#  (riga 4) pacchetti vari (codec audio/video, accesso remoto, log, ltsp, ecc...)
#  (riga 5) pacchetti per la manutenzione
apt install \
  astro-education games-education ezgo-education junior-education \
  blinken calibre cantor chemtool dia einstein fritzing gamine gcompris inkscape kalgebra kalzium kanagram kbruch kgeography khangman kig klettres kmplot kstars ktouch ktuberling kturtle kwordquiz laby lightspeed marble melting parley pencil2d ri-li rocs step tuxmath tuxpaint tuxtype yorick \
  libreoffice libreoffice-l10n-it libreoffice-help-it hunspell-it kolourpaint thunderbird vlc connectagram etoys musescore3 solfege wordpress scratch kdenlive audacity stellarium anki ucblogo cheese scribus pychess geogebra5 blender gimp gimp-help-it vym librecad pdfsam avidemux-qt openshot-qt python3-openshot \
	ubuntu-restricted-extras openvpn cifs-utils smartmontools syslog-ng iftop npm ntp \
	mc vim tmux htop ethstats iptraf-ng nethogs whois

# Exelearning, ex "intef-exe". Precedentemente da ppa:, ora è da snap
sudo snap install exelearning

# chrome
apt install google-chrome-stable
# chromium, è stato spostato su snap anziché apt
snap install chromium
snap install chromium-ffmpeg

################################################################################
########################### Installazione Scratch 3 ############################
################################################################################
# controlla se scratch è già installato
if ! dpkg -s scratch-desktop &> /dev/null; then
  echo
  echo installazione scratch3...
  echo
  wget -t 3 -T 20 -nv -O "scratch3.deb.$$" "${scratch3_deb_url}"
  # Installare il file deb
  if [ $? -eq 0 ]; then
    sudo gdebi "scratch3.deb.$$"
  else
    echo installazione di scratch3 fallita
  fi
  rm "scratch3.deb.$$"
fi

################################################################################
########################### Installazione Freemind #############################
################################################################################
# controlla se freemind è già installato
if [ ! -d /opt/freemind ]; then
  echo
  echo installazione freemind...
  echo
  wget -t 3 -T 20 -nv -O ".freemind_installer.sh.$$" "${freemind_installer_url}"
  chmod +x ".freemind_installer.sh.$$"
  if [ $? -eq 0 ]; then
    ./.freemind_installer.sh.$$
  else
    echo installazione di freemind fallita
  fi
  rm "./.freemind_installer.sh.$$"
fi

################################################################################
########################### Installazione CmapTools ############################
################################################################################
# controlla se cmaptools è già installato
if [ ! -d /opt/cmaptools ]; then
  echo
  echo installazione cmaptools...
  echo
  mkdir .tmp_cmaptools.$$
  pushd .tmp_cmaptools.$$
  wget -t 3 -T 20 -nv -O "cmaptools_installer.tgz" "${cmaptools_installer_url}"
  if [ $? -eq 0 ]; then
    tar xzf "cmaptools_installer.tgz"
    cd cmaptools_installer
    ./install.sh
    if [ $? -ne 0 ]; then
      echo installazione di cmaptools fallita
    fi
  fi
  popd
  rm -rf .tmp_cmaptools.$$
fi


